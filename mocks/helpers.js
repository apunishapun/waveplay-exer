const build = (app, { method, uri, body }) => {
  console.log(`${method}:/${uri}`);
  app[method]('/' + uri, (req, res) => {
    console.log(`\nHandling ${req.method} : ${req.url} \nRESULT:\n${JSON.stringify(body)}\n`);
    res.send(body)
  });
};

const buildMany = (app, data) => {
  Object.keys(data).forEach((id) => {
    build(app, data[id]);
  });
};

module.exports = {
  build,
  buildMany
}