const express = require('express');
const routes = require('./routes');
const PORT = 9991;

const app = express();
app.use(express.json());
routes(app);

console.log('Quewie app mock data server running on http://localhost:' + PORT);
app.listen(PORT);