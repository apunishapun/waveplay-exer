const tests = require('./tests/tests_api');

const routes = (app) => {
  tests(app);
};

module.exports = routes;