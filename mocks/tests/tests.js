const multi = {
  get_tests: {
    method: 'get',
    uri: 'tests',
    body: [
      {
        id: 100,
        title: 'A Wonderful Mock Data Server',
        description: 'This content is from a mock server that feeds the soul of your app.',
      },
      {
        id: 101,
        title: 'A Magnificent Mock Data Server',
        description: 'This content is from a mock server that magnifies the soul of your app.',
      }
    ]
  },
  get_100_tests: {
    method: 'get',
    uri: 'tests/100',
    body: {
      result: {
        data: {
          id: 100,
          title: 'A Wonderful Mock Data Server',
          description: 'This content is from a mock server that feeds the soul of your app.',
        }
      }
    }
  },
  get_101_tests: {
    method: 'get',
    uri: 'tests/100',
    body: {
      result: {
        data: {
          id: 101,
          title: 'A Magnificent Mock Data Server',
          description: 'This content is from a mock server that magnifies the soul of your app.',
        }
      }
    }
  },
  update_100_tests: {
    method: 'put',
    uri: 'tests/100',
    body: {
      result: {
        data: {
          id: 100,
          title: 'A Wonderful Mock Data Server',
          description: 'This content is from a mock server that feeds the soul of your app.',
        }
      }
    }
  },
  get_error_tests: {
    method: 'get',
    uri: 'tests/444',
    body: {
      result: {
        error: {
          title: 'No record found',
          description: 'No existing record found'
        }
      }
    }
  },
}

module.exports = {
  multi
}