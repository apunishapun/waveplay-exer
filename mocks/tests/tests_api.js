const { buildMany } = require('../helpers');
const { multi } = require('./tests');

const runner = (app) => {
  buildMany(app, multi);
  app.post('tests', (req, res) => {
    console.log(`FROM Create: ${req.body}`);
  });
};

module.exports = runner;