const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
  context: __dirname + "/ui",
  output: {
		path: __dirname + "/public",
		filename: "bundle.js"
  },
  module: {
    rules: [
        {
          resolve: {
            alias: {
              actions: path.resolve(__dirname, 'ui/src/actions/'),
              constants: path.resolve(__dirname, 'ui/src/constants/'),
              reducers: path.resolve(__dirname, 'ui/src/reducers/'),
              components: path.resolve(__dirname, 'ui/src/components/'),
              images: path.resolve(__dirname, 'ui/src/images/'),
            }
          },
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
              loader: "babel-loader"
          }
        },
        {
          test: /\.html$/,
          use: [{
              loader: "html-loader"
          }]
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        },
        {
          test: /\.(woff|woff2|eot|ttf|svg)$/,
          use: [{
              loader: 'file-loader'
          }]
        },
        {
          test: /\.(jpg|png)$/,
          use: {
            loader: 'url-loader'
          }
        },
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./index.html",
      filename: "./index.html"
    })
  ]
};