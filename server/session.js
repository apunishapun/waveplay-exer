var session = require('express-session');
var FileStore = require('session-file-store')(session);

var fileStoreOptions = {
  path: './temp/sessions'
};

var expiryDate = new Date(Date.now() + 60000);
const sessionHandler = session({
    name: 'sessionista',
    store: new FileStore(fileStoreOptions),
    secret: '!2ks*3ffeuwps',
    resave: true,
    saveUninitialized: false,
    cookie: {
      httpOnly: true,
      expires: expiryDate
    },
});

const sessionWriter = (req, res, next) => {
  if (req.session.views) {
    req.session.views++;
  } else {
    req.session.views = 1;
    const { ip, hostname, subdomains } = req;
    req.session.clientInfo = { ip, hostname, subdomains };
  }
  next();
};

const SessionManager = (app) => {
  app.use(sessionHandler);
  app.use(sessionWriter);
};

module.exports = SessionManager;