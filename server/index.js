var express = require('express');
const SessionManager = require('./session');
const { test } = require('./routes');
const PORT = 8881;

var app = express();
app.use(express.json());
SessionManager(app);
app.use(express.static('public'));

console.log('Boilerplate app running on port ' + PORT);
app.listen(PORT);