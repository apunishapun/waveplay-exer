import React from 'react';
import { connect } from 'react-redux';
import BoothImg from 'images/waveplay-booth.png';

const Booth = ({ style }) => {
  return (<img style={style} className="booth" style={style} src={BoothImg} />);
};

const mapStateToProps = ({ app }) => {
  const { booth } = app;
  const { cw, ch, top, left, } = booth;
  const style = {
    width: `${cw}px`,
    height: `${ch}px`,
    top: `${top}px`,
    left: `${left}px`,
    position: 'absolute',
    zIndex: '2',
  };
  return { style };
};

export default connect(mapStateToProps)(Booth);