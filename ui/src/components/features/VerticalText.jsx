import React from 'react';
import { connect } from 'react-redux';

const VerticalText = ({ style, text }) => {
  return (
    <div style={style} className="vertical-text">{text}</div>
  );
};

const mapStateToProps = ({ app }) => {
  const { width, height, verticalText } = app;
  const { size, text } = verticalText;
  return {
    style: {
      fontSize: `${size}em`,
      width: `${width}px`,
      top: `${height / 2}px`,
    },
    text,
  };
};

export default connect(mapStateToProps)(VerticalText);