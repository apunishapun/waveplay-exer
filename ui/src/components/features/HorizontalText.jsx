import React from 'react';
import { connect } from 'react-redux';

const HorizontalText = ({ style, text }) => {
  return (
    <div style={style} className="horizontal-text">{text}</div>
  );
};

const mapStateToProps = ({ app }) => {
  const { width, height, horizontalText } = app;
  const { size, text } = horizontalText;
  return {
    style: {
      fontSize: `${size}em`,
      width: `${width}px`,
      top: `${height / 2}px`,
    },
    text,
  };
};

export default connect(mapStateToProps)(HorizontalText);