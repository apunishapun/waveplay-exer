import React from 'react';
import { connect } from 'react-redux';
import BgImg from 'images/waveplay-booth-bg.jpg';

const BoothBg = ({ style }) => {
  return (<img style={style} className="booth-bg" style={style} src={BgImg} />);
};

const mapStateToProps = ({ app }) => {
  const { bg } = app;
  const { cw, ch, top, left } = bg;
  const style = {
    width: `${cw}px`,
    height: `${ch}px`,
    top: `${top}px`,
    left: `${left}px`,
    position: 'absolute',
    zIndex: '1',
  };
  return { style };
};

export default connect(mapStateToProps)(BoothBg);