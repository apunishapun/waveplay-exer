import React, { Component } from 'react';
import { connect } from 'react-redux';
import BoothBg from 'components/features/BoothBg.jsx';
import Booth from 'components/features/Booth.jsx';
import VerticalText from 'components/features/VerticalText.jsx';
import HorizontalText from 'components/features/HorizontalText.jsx';
import { runApp } from 'actions/app';

const defaultStyle = {
  position: 'relative',
  overflow: 'hidden',
};

class Main extends Component {
  componentDidMount() {
    this.props.dispatch(runApp());
  }

  render() {
    const { width, height } = this.props;
    const style = { ...defaultStyle, width, height };
    return (
      <div style={style}>
        <BoothBg />
        <Booth />
        <VerticalText />
        <HorizontalText />
      </div>
    );
  }
}

const mapstateToProps = ({ app }) => {
  const { width, height } = app;
  return { width: `${width}px`, height: `${height}px` };
};

export default connect(mapstateToProps)(Main);