import {
  ADJUST_WINDOW,
  ADJUST_BOOTH,
  ADJUST_BG,
} from 'constants/actions/app';

const initState = {
  width: window.innerWidth,
  height: window.innerHeight,
  bg: {
    dw: 1366,
    dh: 1690,
    cw: 0,
    ch: 0,
    scale: 0,
    top: 0,
    left: 0,
  },
  booth: {
    dw: 884,
    dh: 644,
    cw: 0,
    ch: 0,
    scale: 0,
    top: 0,
    left: 0,
  },
  verticalText: {
    text: 'Lorem Ipsum Dolor',
    defaultSize: 3.5,
    size: 3.5,
  },
  horizontalText: {
    text: 'Sit Ametip Ipsum Lorem',
    defaultSize: 3,
    size: 3,
  },
};

const adjustText = ({ scale }, old) => {
  const { defaultSize } = old;
  return {
    ...old,
    size: defaultSize * scale,
  };
};

const app = (state = initState, action) => {
  console.log(action);
  
  const { type, width, height, newBooth, newBg } = action;
  switch (type) {
    case ADJUST_WINDOW:
      return { ...state, width, height };
    case ADJUST_BOOTH:
      return {
        ...state,
        booth: { ...newBooth },
        verticalText: adjustText(newBooth, state.verticalText),
        horizontalText: adjustText(newBooth, state.horizontalText),
      };
    case ADJUST_BG:
      return { ...state, bg: { ...newBg } };
    default:
      return state;
  }
}

export default app;