import {
  ADJUST_WINDOW,
  ADJUST_BOOTH,
  ADJUST_BG,
} from 'constants/actions/app';

const isScreenChanged = ({ width, height }) => {
  return width !== window.innerWidth || height !== window.innerHeight;
};

const isPortrait = () => {
  return window.innerHeight > window.innerWidth;
};

const getScale = (dw, dh) => {
  if (isPortrait()) {
    return window.innerWidth / dw;
  }
  return window.innerHeight / dh;
};

const getNewSize = (scale, dw, dh, pad = 0) => {
  return {
    dw, dh,
    cw: (dw - pad) * scale,
    ch: (dh - pad) * scale,
    scale,
  };
};

const adjustBg = () => (dispatch, getState) => {
  const { booth, bg, width, height } = getState().app;
  const { dw, dh } = bg;
  const { scale } = booth;
  const newBg = getNewSize(scale, dw, dh);
  newBg.left = (width / 2) - (newBg.cw / 2);
  newBg.top = (height / 2) - (newBg.ch / 2) - (newBg.ch * .08);
  dispatch({ type: ADJUST_BG, newBg });
};

const adjustWindow = () => (dispatch) => dispatch({ type: ADJUST_WINDOW, height: window.innerHeight, width: window.innerWidth});

const adjustBooth = () => (dispatch, getState) => {
  const { booth, width, height } = getState().app;
  const { dw, dh } = booth;
  const scale = getScale(dw, dh);
  const newBooth = getNewSize(scale, dw, dh, 20);
  newBooth.left = (width / 2) - (newBooth.cw / 2);
  newBooth.top = (height / 2) - (newBooth.ch / 2);
  dispatch({ type: ADJUST_BOOTH, newBooth });
  dispatch(adjustBg());
};

export const resizeImages = (dispatch, getState, forceRun = false) => {
  const { app } = getState();
  if (forceRun || isScreenChanged(app)) {
    dispatch(adjustWindow());
    dispatch(adjustBooth());
  }
};