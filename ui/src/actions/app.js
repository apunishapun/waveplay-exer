import { resizeImages } from './resizer';

const appLoop = (dispatch, getState) => {
  resizeImages(dispatch, getState);
};

const runApp = (fps = 30) => (dispatch, getState) => {
  resizeImages(dispatch, getState, true);
  setInterval(() => appLoop(dispatch, getState), 1000 / fps);
};

export {
  runApp
}